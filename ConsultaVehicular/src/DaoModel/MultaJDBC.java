
package DaoModel;

import DtoModel.Multa;
import data.ConexionDB;
import java.sql.Connection;
import java.sql.SQLException;
import static data.ConexionDB.close;
import static data.ConexionDB.getConecction;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;



public class MultaJDBC {
    
    private static final String SQL_SELECT = "SELECT folio,motivo,precio,placa,estado FROM multa";
    //private static final String SQL_UPDATE = "UPDATE multa SET motivo = ? WHERE  folio = ?";
    private static final String SQL_UPDATE = "UPDATE multa SET estado = ? WHERE  folio = ?";
    
     public List<Multa> mostrarDatos() {
        Connection coneccion = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Multa multa = null;
        List<Multa> listaMultas = new ArrayList<>(); 
        try {
            coneccion =  getConecction();
            stmt = coneccion.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();
            while(rs.next()){
                int folio     = rs.getInt("folio");
                String motivo = rs.getString("motivo");
                float precio  = rs.getFloat("precio");
                String placa  = rs.getString("placa");
                String estado = rs.getString("estado");
                multa = new Multa(folio,motivo,precio,placa,estado);
                listaMultas.add(multa);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            try {
            ConexionDB.close(rs);
            ConexionDB.close(stmt);
            ConexionDB.close(coneccion);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
        return listaMultas;
    }
    
    // public void actualizarMulta(){
    // stmt.setInt(2,multa.getPlaca());
    public void actualizarMulta(int folio){
        Connection coneccionUpdate = null;
        PreparedStatement stmt = null;
        int eliminados = 0;
        try {
            coneccionUpdate = getConecction();
            stmt = coneccionUpdate.prepareStatement(SQL_UPDATE);
            stmt.setString(1,"PAGADA");
            stmt.setInt(2,folio);
            eliminados = stmt.executeUpdate();
            
        } catch (Exception e) {
          e.printStackTrace(System.out);
        } finally{
            try {
                close(stmt);
                close(coneccionUpdate);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
    
    }
}
