package DaoModel;

import DtoModel.Tenencia;
import data.ConexionDB;
import java.sql.Connection;
import java.sql.SQLException;
import static data.ConexionDB.close;
import static data.ConexionDB.getConecction;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TenenciaJDBC {
    
    String cadena;
    
    private static final String SQL_SELECT = "SELECT linea_captura,placa,ejercicio,impuesto,derechos FROM tenencia";
    //private static final String SQL_UPDATE = "UPDATE multa SET motivo = ? WHERE  folio = ?";
    private static final String SQL_UPDATE = "UPDATE tenencia SET linea_captura = ? WHERE  placa = ?";
    
     public List<Tenencia> mostrarDatos() {
        Connection coneccion = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Tenencia tenencia = null;
        List<Tenencia> listaTenencias = new ArrayList<>(); 
        try {
            coneccion =  getConecction();
            stmt = coneccion.prepareStatement(SQL_SELECT);
            rs = stmt.executeQuery();
            while(rs.next()){
                cadena = rs.getString("linea_captura");
                String linea_captura     = rs.getString("linea_captura");
                String placa = rs.getString("placa");
                int ejercicio  = rs.getInt("ejercicio");
                int impuesto  = rs.getInt("impuesto");
                int derechos = rs.getInt("derechos");
                tenencia = new Tenencia(linea_captura,placa,ejercicio,impuesto,derechos);
                listaTenencias.add(tenencia);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
        finally{
            try {
            ConexionDB.close(rs);
            ConexionDB.close(stmt);
            ConexionDB.close(coneccion);
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
        return listaTenencias;
    }
    
    // public void actualizarMulta(){
    // stmt.setInt(2,multa.getPlaca());
    public void actualizarTenencia(String placa){
        Connection coneccionUpdate = null;
        PreparedStatement stmt = null;
        int eliminados = 0;
        try {
            
            cadena.substring(0,5);
            
            coneccionUpdate = getConecction();
            stmt = coneccionUpdate.prepareStatement(SQL_UPDATE);
            stmt.setString(1,cadena+"-TP");
            stmt.setString(2,placa);
            eliminados = stmt.executeUpdate();
            
        } catch (Exception e) {
          e.printStackTrace(System.out);
        } finally{
            try {
                close(stmt);
                close(coneccionUpdate);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
    
    }
}
