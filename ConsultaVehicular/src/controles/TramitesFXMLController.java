/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package controles;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author ivano
 */
public class TramitesFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private Button btn5;
    @FXML
    private Button btn6;
    @FXML
    private ChoiceBox<String> cbLicencia;
    @FXML
    private ChoiceBox<String> cbPermisos;
    @FXML
    private ChoiceBox<String> cbTramites;
    
    private final String[] licencia = {"Licencia A automovilista y motociclista Permanente",
                                        "Licencia A1 motociclistas por 3 años",
                                        "Licencia A2 vehiculos particulares y motociclista",
                                        "Licencia Tarjetón B de taxis 2 años",
                                        "Licencia Tarjetón B de taxis 3 años",
                                        "Tarjetón C pasajeros, D carga, E emergencia por 2 años",
                                        "Tarjetón C pasajeros, D carga, E emergencia por 3 años",
                                        "Licencias A, B, C, D, E o permisos de conducir",
                                        "Permiso de Conducir con Vigencia Única"};
    
    private final String[] permisos = {"Permiso de carga ocasional por siete días",
                                       "Permiso para circular sin placas",
                                       "Por cambio de propietario, carrocería, motor",
                                       "Cambio de domicilio y corrección de datos",
                                       "Cualquier otro permiso que no exceda 90 días",
                                       "Por reposición de calcomanía"};
    
    private final String[] tramites = {"Baja o suspensión de vehiculo",
                                       "Reposicion de placas Servicio publico",
                                       "Alta Servicio Público de Transporte",
                                       "Servicio privado de transporte",
                                       "Arrastre hasta 3.5tons",
                                       "Arrastre mayor a 3.5tons"};
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      cboxPermisos();
      cboxLicencia();      
      cboxTramites();
    }
    
    @FXML
    public void regresarVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/BuscaPlacaFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    BuscaPlacaFXMLController controlador = (BuscaPlacaFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            licenciaPrecios();
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }

    public void cboxLicencia(){
        //Agrega meses al ChoiceBox
        cbLicencia.getItems().addAll(licencia);
       //Se agrega un titulo al choiceBox
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbLicencia.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Licencias");
                }
                return;
            }
        }
    }); 
    }
        
    public void cboxPermisos(){
        //Agrega meses al ChoiceBox
        cbPermisos.getItems().addAll(permisos);
       //Se agrega un titulo al choiceBox
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbPermisos.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Permisos");
                }
                return;
            }
        }
    });    
    }
    
    public void cboxTramites(){
        //Agrega meses al ChoiceBox
        cbTramites.getItems().addAll(tramites);
       //Se agrega un titulo al choiceBox
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbTramites.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Tramites");
                }
                return;
            }
        }
        
    });    
    }
    
    @FXML
    public void tramiteEmpresa(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://www.semovi.cdmx.gob.mx/tramites_de_control_vehicular/tramites-para-empresas"));
    }
    
    @FXML
    public void controlVehicular(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://semovi.cdmx.gob.mx/tramites_de_control_vehicular"));
    }
    
    @FXML
    public void tarjetaCirculacion(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://semovi.cdmx.gob.mx/tramites-y-servicios/vehiculos-particulares/automovil/control-vehicular-y-tarjeta-de-circulacion/tarjeta-de-circulacion"));
    }

    @FXML
    public void bolsaTrabajo(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://www.semovi.cdmx.gob.mx/servicio-social-y-bolsa-de-trabajo/bolsa-de-trabajo"));
    }

    @FXML
    public void cursosVehiculares(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://www.semovi.cdmx.gob.mx/tramites-y-servicios/cursos-vehiculares"));
    }
    
    @FXML
    public void vehiculoCarga(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://www.semovi.cdmx.gob.mx/tramites-y-servicios/transporte-de-carga/tramites/revista-de-carga-2021"));
    }

    @FXML
    public void licenciaPrecios(){
        String cbox=cbLicencia.getValue();
        
        if(recuperaItem2().equals("Licencia A automovilista y motociclista Permanente")){
            JOptionPane.showMessageDialog(null,"Costo: $450.00","Información",JOptionPane.WARNING_MESSAGE);           
        }else if(recuperaItem2().equals("Licencia A1 motociclistas por 3 años")){
            JOptionPane.showMessageDialog(null, "Costo: $900.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Licencia A2 vehiculos particulares y motociclista")){
            JOptionPane.showMessageDialog(null, "Costo: $900.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Licencia Tarjetón B de taxis 2 años")){
            JOptionPane.showMessageDialog(null, "Costo: $1,117.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Licencia Tarjetón B de taxis 3 años")){
            JOptionPane.showMessageDialog(null, "Costo: $1,681.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Tarjetón C pasajeros, D carga, E emergencia por 2 años")){
            JOptionPane.showMessageDialog(null, "Costo: $1,619.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Tarjetón C pasajeros, D carga, E emergencia por 3 años")){
            JOptionPane.showMessageDialog(null, "Costo: $2,431.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Licencias A, B, C, D, E o permisos de conducir")){
            JOptionPane.showMessageDialog(null, "Costo: $205.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem2().equals("Permiso de Conducir con Vigencia Única")){
            JOptionPane.showMessageDialog(null, "Costo: $462.00","Información",JOptionPane.WARNING_MESSAGE);
        }
    }
    
    @FXML
    public void permisosPrecios(){
        String cbox=cbPermisos.getValue();
        
        if(recuperaItem3().equals("Permiso de carga ocasional por siete días")){
            JOptionPane.showMessageDialog(null, "Costo: $110.00","Información",JOptionPane.WARNING_MESSAGE);           
        }else if(recuperaItem3().equals("Permiso para circular sin placas")){
            JOptionPane.showMessageDialog(null, "Costo: $233.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem3().equals("Por cambio de propietario, carrocería, motor")){
            JOptionPane.showMessageDialog(null, "Costo: $340.50","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem3().equals("Cambio de domicilio y corrección de datos")){
            JOptionPane.showMessageDialog(null, "Costo: $340.50","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem3().equals("Cualquier otro permiso que no exceda 90 días")){
            JOptionPane.showMessageDialog(null, "Costo: $340.50","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem3().equals("Por reposición de calcomanía")){
            JOptionPane.showMessageDialog(null, "Costo: $233.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(cbPermisos.equals("Permisos")){
            System.out.println("No se selecciono ninguna opcion");
        }
    }
    
    @FXML
    public void tramitesPrecios(){
        String cbox=cbTramites.getValue();
        
        if(recuperaItem().equals("Baja o suspension de vehiculo")){
            JOptionPane.showMessageDialog(null, "Costo: $695.00","Información",JOptionPane.WARNING_MESSAGE);           
        }else if(recuperaItem().equals("Reposicion de placas Servicio publico")){
            JOptionPane.showMessageDialog(null, "Costo: $1,126.00 por placa","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem().equals("Alta Servicio Público de Transporte")){
            JOptionPane.showMessageDialog(null, "Costo: $1,586.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem().equals("Servicio privado de transporte")){
            JOptionPane.showMessageDialog(null, "Costo: $1,504.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem().equals("Arrastre hasta 3.5tons")){
            JOptionPane.showMessageDialog(null, "Costo: $788.00","Información",JOptionPane.WARNING_MESSAGE);
        }else if(recuperaItem().equals("Arrastre mayor a 3.5tons")){
            JOptionPane.showMessageDialog(null, "Costo: $1,571.00","Información",JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public String recuperaItem(){
        String itm=cbTramites.getValue();
        return itm;
    }
    
    public String recuperaItem2(){
        String itm=cbLicencia.getValue();
        return itm;
    }
    
    public String recuperaItem3(){
        String itm=cbPermisos.getValue();
        return itm;
    }
}
