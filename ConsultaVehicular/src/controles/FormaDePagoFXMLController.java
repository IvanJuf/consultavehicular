package controles;

import DaoModel.MultaJDBC;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class FormaDePagoFXMLController implements Initializable {

    @FXML
    private AnchorPane ap;
    @FXML
    private ImageView imgPaypal;
    @FXML
    private ImageView imgTarjeta;
    @FXML
    private Label lbTitulo;
    @FXML
    private Button btnRegresar;
    
    private String placa;
    int cont;
    double p;
    int fl;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    public void pagoTarjeta(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MenuTarjetasFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MenuTarjetasFXMLController controlador = (MenuTarjetasFXMLController)loader.getController();
             
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            controlador.reciboPrecio(p);
            String pr = String.valueOf(p);
           // controlador.setLbPrecio(pr);
            controlador.reciboFolio(fl);
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multa = new MultaJDBC();
            //System.out.println(multa.mostrarDatos());
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void pagoPayPal(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/PagoPayPalFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    PagoPayPalFXMLController controlador = (PagoPayPalFXMLController)loader.getController();
             
            controlador.reciboPrecio(p);
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            String pr = String.valueOf(p);
            controlador.setLbPrecio(pr);

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multa = new MultaJDBC();
            //System.out.println(multa.mostrarDatos());
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MultasFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MultasFXMLController controlador = (MultasFXMLController)loader.getController();
                        
            //controlador.placaStage1(multa.getPlaca().toUpperCase());
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    } 
    
    public void reciboPlaca(String p){
        this.placa=p;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
    
    public void reciboPrecio(double prc){
        this.p = prc;
    }
    
    public void reciboFolio(int f){
        this.fl =f;
    }
}
