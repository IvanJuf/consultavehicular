package controles;

import DaoModel.MultaJDBC;
import DtoModel.Auto;
import DtoModel.Multa;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class MenuFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;
    
    private Auto auto = null;
    
    private String placa;
    
            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    } 
    
    @FXML
    public void ventanaMultas(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MultasFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MultasFXMLController controlador = (MultasFXMLController)loader.getController();
            
            controlador.cargaTabla(placa);
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            /*
            MultaJDBC multa = new MultaJDBC();
            System.out.println(multa.mostrarDatos());
		*/	      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }

    @FXML
    public void ventanaTenencias(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/TenenciasFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    TenenciasFXMLController controlador = (TenenciasFXMLController)loader.getController();
            
            controlador.cargaTabla(placa);
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            /*
            MultaJDBC multa = new MultaJDBC();
            System.out.println(multa.mostrarDatos());
		*/	      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void ventanaCitas(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/CitasFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    CitasFXMLController controlador = (CitasFXMLController)loader.getController();
            
	    controlador.reciboPlaca(placa);
            controlador.setLbPlaca(placa);
            ap.getChildren().clear();
	    ap.getChildren().add(root);
            	      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void salir(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/BuscaPlacaFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    BuscaPlacaFXMLController controlador = (BuscaPlacaFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    MultaJDBC listaMultasJDBC = new MultaJDBC();
    private ObservableList<Multa> listaMultas = null;
    
    public void agregarAuto(String buscar){     //Aqui recibo la placa ***************
        this.placa = buscar;
        MultaJDBC listaMultasJDBC = new MultaJDBC();
        listaMultas = FXCollections.observableArrayList();
        
        for(Multa listaMulta : listaMultasJDBC.mostrarDatos()){
            listaMultas.add(listaMulta);
        }
    }
    
}
