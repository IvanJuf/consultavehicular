package controles;

import DaoModel.MultaJDBC;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class MenuTarjetasFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;
    
    private String placa;
    int cont;
    double p;
    int fl;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    @FXML
    public void pagoCredito(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/PagoTarjetaFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    PagoTarjetaFXMLController controlador = (PagoTarjetaFXMLController)loader.getController();
             
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            String pr = String.valueOf(p);
            controlador.lbPrecio(pr);
            controlador.reciboFolio(fl);
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multa = new MultaJDBC();
            //System.out.println(multa.mostrarDatos());
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void pagoDebito(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/PagoTarjetaFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    PagoTarjetaFXMLController controlador = (PagoTarjetaFXMLController)loader.getController();
             
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            String pr = String.valueOf(p);
            controlador.lbPrecio(pr);
            controlador.reciboFolio(fl);
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multa = new MultaJDBC();
            //System.out.println(multa.mostrarDatos());
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/FormaDePagoFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    FormaDePagoFXMLController controlador = (FormaDePagoFXMLController)loader.getController();
                        
            //controlador.cargaTabla(multa.getPlaca().toUpperCase());
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    public void reciboPlaca(String p){
        this.placa=p;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
    
    public void reciboPrecio(double prc){
        this.p = prc;
    }
    
    public void reciboFolio(int f){
        this.fl =f;
    }
    
}
