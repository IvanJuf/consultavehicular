package controles;


import DaoModel.MultaJDBC;
import DaoModel.TenenciaJDBC;
import DtoModel.Multa;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
//import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
//import javax.swing.JOptionPane;

public class BuscaPlacaFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;
    @FXML
    private Button btnConsulta;
    @FXML
    private ImageView imgFondo;
    @FXML
    private Label lbTitulo;
    @FXML
    private Label lbPlaca;
    @FXML
    private TextField tfBusca;
    @FXML
    private Label lbIndicacion;
    @FXML
    private Button btnBusca;
    
    Multa multa;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        toolTip();
    }

    @FXML
    public void ventanaMenu() throws SQLException{
        try {
            if (validarPlaca() == true) {
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MenuFXML.fxml"));
                Parent root = loader.load();
                MenuFXMLController controlador = loader.getController();
                controlador.agregarAuto(tfBusca.getText().toUpperCase()); //****   aqui mando la placa a menu
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
                Stage nuevoStage = (Stage) btnBusca.getScene().getWindow();
                nuevoStage.close();
                
                MultaJDBC multa = new MultaJDBC();
                TenenciaJDBC tenencia = new TenenciaJDBC();
               // System.out.print(multa.mostrarDatos());
                
            } else {
                JOptionPane.showMessageDialog(null, "La placa no se encontro en el padrón");
            }

        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }
    }
    
    @FXML
    public void ventanaTramites(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/TramitesFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    TramitesFXMLController controlador = (TramitesFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    //Metodo para mostrar mensaje "consulta de tramites"
    public void toolTip(){
        Tooltip tooltip1 = new Tooltip("Consulta otros tramites");
        btnConsulta.setTooltip(tooltip1);
    }
    
    private boolean validarPlaca() throws SQLException {
        MultaJDBC multaJDBC = new MultaJDBC();
        List<Multa> nuevaListaAutos = multaJDBC.mostrarDatos();
        for (Multa nuevaListaAuto : nuevaListaAutos) {
            if (nuevaListaAuto.getPlaca().equals(tfBusca.getText().toUpperCase())) {
                return true;
            }
        }
        return false;
    }    
    
}
