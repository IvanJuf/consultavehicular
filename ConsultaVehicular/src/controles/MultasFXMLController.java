package controles;

import DaoModel.MultaJDBC;
import DtoModel.Multa;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class MultasFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;
    @FXML
    private TableView<Multa> tvTabla;
    @FXML
    private TableColumn<Multa, Integer> tcFolio;
    @FXML
    private TableColumn<Multa, String> tcDetalles;
    @FXML
    private TableColumn<Multa, Float> tcMonto;
    @FXML
    private TableColumn<Multa, String> tvEstatus;
    @FXML
    private Label lbTitulo;
    @FXML
    private Button btnPagar;
    @FXML
    private Button btnRegresar;

    Multa multa;
    private String placa;
    int cont=1;
    String m;
    
    MultaJDBC listamultasJDBC = new MultaJDBC();
    private ObservableList<Multa> listaMultas = null;

    
    public void cargaTabla(String txtBuscar){
        this.placa = txtBuscar;
        MultaJDBC listamultasJDBC = new MultaJDBC();
        listaMultas = FXCollections.observableArrayList();
        for (Multa listaMulta : listamultasJDBC.mostrarDatos()) {
            if(placa.equals(listaMulta.getPlaca())){
                listaMultas.add(listaMulta);               
            }
            
        }  

        tcFolio.setCellValueFactory(new PropertyValueFactory<>("folio"));
        tcDetalles.setCellValueFactory( new PropertyValueFactory<>("motivo"));
        tcMonto.setCellValueFactory( new PropertyValueFactory<>("precio"));
        tvEstatus.setCellValueFactory(new PropertyValueFactory<>("estado"));
        tvTabla.setItems(listaMultas);
                
    }
 

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tvTabla.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {this.multa = newValue;});
        
    }
    

    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MenuFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MenuFXMLController controlador = (MenuFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void pagoEnLinea(){
        String et= multa.getEstado();
        
        if(et.equals("PAGADA")){
            JOptionPane.showMessageDialog(null, "Esta multa ya fue pagada","Atención",JOptionPane.WARNING_MESSAGE);         
        }else{
            try{
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/FormaDePagoFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    FormaDePagoFXMLController controlador = (FormaDePagoFXMLController)loader.getController();
            
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            controlador.reciboPrecio(multa.getPrecio());
            
            int folio = multa.getFolio();
            controlador.reciboFolio(folio);
            
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multas = new MultaJDBC();
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
        }    
    }
    
    @FXML
    public void multaPagada() {
        
        float p = multa.getPrecio();
        System.out.println("Precio 1---->"+p);
        this.cont++;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
    
    
       
}
