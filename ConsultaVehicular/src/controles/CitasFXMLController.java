package controles;

import DtoModel.Cita;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

public class CitasFXMLController implements Initializable{
    
    @FXML
    private Label lbPlaca;
    @FXML
    private ChoiceBox cbDelegacion;
    @FXML
    private ChoiceBox cbVerificentro;
    @FXML
    private ChoiceBox cbHorario;
    @FXML
    private DatePicker dpCalendario;
    @FXML
    private AnchorPane ap;
    
    private String placa;
  
    
    private final String[] delegacion = {"Tlahuac","Coyoacán","Alvaro Obregón","Azcapotzalco","Benito Juárez",
                                         "Cuajimalpa","Cuauhtemoc","Gustavo A. Madero","Iztapalapa","Miguel Hidalgo",
                                         "Tlalpan","Venustiano Carranza","Xochimilco"};

    private final String[] tlahuac = {"SUMINISTROS Y MAQUINARIA KEBEC, S.A. DE C.V.","SOLUCIONES EMPRESARIALES DRAKO, S DE R.L. DE C.V."};
    
    private final String[]  coyoacan ={"ARTE AUTOMOTRÍZ, S. DE R.L. DE C.V.","CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V.",
                                       "COMPAÑÍA EMPRESARIAL VEDOZA, S.A. DE C.V.","GRUPO ORNUTO S.A. DE C.V.",
                                       "NOVA AMBIENTAL, S.A. DE C.V","CONTROL ECOLÓGICO COYOACÁN, S.A. DE C.V."};
    
    private final String[] alvaro = {"CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V.","CIUDAD MAXIPLUS, S. A. DE C. V.",
                                     "IMPULSORA ECOLÓGICA SANTA FE S.A. DE C.V..","ASEROEMPRESA, S.A DE C.V."};
    
    private final String[] azcapotzalco={"VERIFICACIÓN INTEGRAL DE EMISIONES, S.A. DE C.V.","POWER CARS MUNDO, S. A. DE C. V.",
                                         "EXCELENCIA REFACCIONARIA AVE FENIX, S. A. DE C. V..","CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V.",
                                         "ESTACIONAMIENTO RICE, S. A.","VERIFICENTRO EL ROSARIO, S.A. DE C.V."};
    
    private final String[] benito = {"EXCELENCIA REFACCIONARIA AVE FENIX, S. A. DE C. V.","CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V.",
                                      "VICENTE SERGIO PEREA FLORES","VERIFICENTRO SIGLO XXI, S.A. DE C.V."};
    
    private final String[] cuajimalpa={"CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V.","GRUPO CONTADERO, S. A. DE C. V."};
    
    private final String[] cuauhtemoc={"ECO AMBIENTAL CUAUHTÉMOC, S.A DE C.V.","VERIFICENTRO, S.A. DE C.V.",
                                       "VERIFICENTROS PROVIDA, S. A. DE C. V.","GESTIÓN DEL AIRE, S.A. DE C.V.",
                                       "DIAGNOSTICO DE EMISIONES, S.A. DE C.V.","DESARROLLO Y ASESORÍA APLICADA NOVO, S.A. DE C.V."};
            
    private final String[] gustavo ={"MAC 1, S.A DE C.V.","RONI, S.A DE C.V.",
                                     "IMPULSORA ECOLÓGICA BARRAGÁN, S.A. DE C.V.","INGENIERÍA Y VERIFICACIÓN PANTITLÁN, S.A. DE C.V.",
                                     "JOSÉ RAFAEL OCAÑA UMBRAL","DINÁMICA EN EQUIPO INDUSTRIAL, S.A DE C.V.",
                                     "ECOLOGÍA AMBIENTAL COPILCO, S.A. DE C.V.","CERVITEC, SERVICIO TÉCNICO ECOLÓGICO, S.A. DE C.V."};
    
    private final String[] iztacalco ={"INGENIERÍA Y VERIFICACIÓN PANTITLÁN, S.A. DE C.V."," POWER CARS MUNDO, S. A. DE C. V.",
                                       "CIUDAD MAXIPLUS, S. A. DE C. V.","SUMINISTROS Y MAQUINARIA KEBEC, S.A. DE C.V.",
                                       "DESARROLLO Y ASESORÍA APLICADA NOVO, S.A. DE C.V."};
            
    private final String[] iztapalapa ={"POWER CARS MUNDO, S. A. DE C. V.","EXCELENCIA REFACCIONARIA AVE FENIX, S. A. DE C. V.",
                                        "DISTRIBUIDORA MIVEP S.A. DE C.V.","CIUDAD MAXIPLUS, S. A. DE C. V..",
                                        "MAC 2, S.A DE C.V.","MAC 3, S.A DE C.V.",
                                        "MAC 4, S.A DE C.V.","COMPAÑÍA EMPRESARIAL VEDOZA, S.A. DE C.V.",
                                        "MEDINDUSTRIAS, S.A. DE C.V.","SOLUCIONES EMPRESARIALES DRAKO, S DE R.L. DE C.V.",
                                        "SISTEMA AUTOMOTRIZ DE EXCELENCIA, S.A. DE C.V."};
            
    private final String[] miguel ={"CONTROL ATMOSFÉRICO DE MÉXICO, S.A. DE C.V. ","EXCELENCIA REFACCIONARIA AVE FENIX, S. A. DE C. V.",
                                    "JET VAN CAR RENTAL S.A. DE C.V.","SERVICIO DE MEDICIÓN ORGANIZADA DE GASES, S. A. DE C. V",
                                    "PURO AIRE, S.A. DE C.V.","VERIFICENTRO SAN JOAQUIN, S. A. DE C.V."};
            
    private final String[] tlalpan ={"MARÍA CAROLINA AGOFF","MECANICA AMBIENTAL, S.A. DE C.V."};
            
    private final String[] carranza={"VERIFICACIONES CENTENARIO, S.A. DE C.V."," ALEJANDRO MARTÍNEZ BRIBIESCA"};
            
   private final String[] xochimilco={"VERIFICACIONES CENTENARIO, S.A. DE C.V.","CIUDAD MAXIPLUS, S. A. DE C. V.",
                                      "CONSULTORÍA PRO AMBIENTAL, S.A. DE C.V.","TECNO AMBIENTAL NATIVITAS, S.A. DE C.V."};
   
   private final String[] horario={"9:00","9:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30","15:00"};
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        llenaCbox();
    }
    
    public void llenaCbox(){
        //Agrega meses al ChoiceBox
        cbDelegacion.getItems().addAll(delegacion);
        //Agrega horarios
        cbHorario.getItems().addAll(horario);
       //Se agrega un titulo al choiceBox
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbDelegacion.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Delegación");
                }
                return;
            }
        }
    });
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbVerificentro.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Verificentros");
                }
                return;
            }
        }
    }); 
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbHorario.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Disponibilidad");
                }
                return;
            }
        }
    });        
}
    @FXML    
    public void llenaVerificentro(){
        if(cbDelegacion.getValue()=="Tlahuac"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(tlahuac);
        }else if(cbDelegacion.getValue()=="Coyoacán"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(coyoacan);
        }else if(cbDelegacion.getValue()=="Alvaro Obregón"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(alvaro);
        }else if(cbDelegacion.getValue()=="Azcapotzalco"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(azcapotzalco);
        }else if(cbDelegacion.getValue()=="Benito Juárez"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(benito);
        }else if(cbDelegacion.getValue()=="Cuajimalpa"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(cuajimalpa);
        }else if(cbDelegacion.getValue()=="Cuauhtemoc"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(cuauhtemoc);
        }else if(cbDelegacion.getValue()=="Gustavo A. Madero"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(gustavo);
        }else if(cbDelegacion.getValue()=="Iztapalapa"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(iztapalapa);
        }else if(cbDelegacion.getValue()=="Miguel Hidalgo"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(miguel);
        }else if(cbDelegacion.getValue()=="Tlalpan"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(tlalpan);
        }else if(cbDelegacion.getValue()=="Venustiano Carranza"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(carranza);
        }else if(cbDelegacion.getValue()=="Xochimilco"){
            cbVerificentro.getItems().clear();
            cbVerificentro.getItems().addAll(xochimilco);
        }else{
            System.out.println("Opción incorrecta");
        }
    }
    
    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MenuFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MenuFXMLController controlador = (MenuFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }

    @FXML
    public void agendaCita(){
        Cita cita = new Cita();
        String del = (String) cbDelegacion.getValue();
        String ver = (String) cbVerificentro.getValue();
        String h = (String) cbHorario.getValue();
        String f = dpCalendario.getValue().format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
                
        cita.creaCita(del,ver,h,f);
        cita.crearPDF(del,ver,h,f);
        JOptionPane.showMessageDialog(null, "Cita Agendada","Sistema de Pagos",JOptionPane.WARNING_MESSAGE);
    }
    
    public void reciboPlaca(String p){
        this.placa=p;
    }

    public void setLbPlaca(String lbP) {
        this.lbPlaca.setText(lbP);
    }
      
    
}
