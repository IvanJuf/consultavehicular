package controles;

import DaoModel.MultaJDBC;
import DtoModel.Multa;
import DtoModel.ValidaDatos;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;


public class PagoPayPalFXMLController implements Initializable{
    
    @FXML
    private AnchorPane ap;
    @FXML
    private Button btnPagar;
    @FXML
    private Button btnReg;
    @FXML
    private Label lbPrecio;
    @FXML
    private PasswordField pfPass;
    @FXML
    private TextField tfCorreo;
    
    private String placa;
    Multa multa;
    int cont;
    double p;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/FormaDePagoFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    FormaDePagoFXMLController controlador = (FormaDePagoFXMLController)loader.getController();
                        
            //controlador.placaStage1(multa.getPlaca().toUpperCase());
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            System.out.println("Precio PayPal--->"+p);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    private void validaCuenta() {
        if (ValidaDatos.validarCorreo(tfCorreo.getText()) && ValidaDatos.validarPassword(pfPass.getText())) {
            
            JOptionPane.showMessageDialog(null, "Procesando pago", "Sistema de Pagos", JOptionPane.WARNING_MESSAGE);
            MultaJDBC multaPagada = new MultaJDBC();
            multaPagada.actualizarMulta(multa.getFolio());
            regresaVentanaListaMultas();
        }
        if (tfCorreo.getText().isEmpty() || ValidaDatos.validarCorreo(tfCorreo.getText()) == false) {
            tfCorreo.setText(null);
            tfCorreo.setStyle("-fx-prompt-text-fill: red;");
            tfCorreo.setPromptText("*Campo incorrecto");
        }
        if (pfPass.getText().isEmpty() || ValidaDatos.validarPassword(pfPass.getText()) == false) {
            pfPass.setText(null);
            pfPass.setStyle("-fx-prompt-text-fill: red;");
            pfPass.setPromptText("*Campo incorreto");
        }
    }
    
   @FXML
    public void multaPagada(){ 
            MultaJDBC multaPagada = new MultaJDBC();
            multaPagada.actualizarMulta(multa.getFolio());
            regresaVentanaListaMultas();
    }

    public void regresaVentanaListaMultas() {
        try {
            
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MultasFXML.fxml"));
                Parent root = loader.load();
                MultasFXMLController controlador = loader.getController();

                controlador.cargaTabla(placa);
                controlador.reciboCont(cont);
                
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
                Stage nuevoStage = (Stage) btnPagar.getScene().getWindow();
                nuevoStage.close();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
    
     @FXML
    public void registraPaypal(ActionEvent event) throws URISyntaxException, IOException{
        Desktop.getDesktop().browse
        (new URI("https://www.paypal.com/mx/webapps/mpp/account-selection"));
    }
    
    public void reciboPlaca(String p){
        this.placa=p;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
    
    public void reciboPrecio(double prc){
        this.p=prc;
    }
    
    //Metodo para mostra el precio de la placa al inicializar la vista
    public void setLbPrecio(String lbPrecio) {
        this.lbPrecio.setText("$ "+lbPrecio);
    }  
}
