package controles;

import DaoModel.MultaJDBC;
import DaoModel.TenenciaJDBC;
import DtoModel.Tenencia;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

public class TenenciasFXMLController implements Initializable {
    
    @FXML
    private AnchorPane ap;   
    @FXML
    private Button btnPagar;
    @FXML
    private Button btnRegresar;
    @FXML
    private TableView<Tenencia> tvTenencias;
    @FXML
    private TableColumn<Tenencia, String> tcCaptura;
    @FXML
    private TableColumn<Tenencia, Integer> tcAdeudo;
    @FXML
    private TableColumn<Tenencia, Integer> tcImpuesto;
    @FXML
    private TableColumn<Tenencia, Integer> tcDerechos;
    
    Tenencia tenencia;
    private String placa;
    int cont=2;   
    
    TenenciaJDBC listatenenciasJDBC = new TenenciaJDBC();
    private ObservableList<Tenencia> listaTenencias = null;

    
    public void cargaTabla(String txtBuscar){
        this.placa = txtBuscar;
        TenenciaJDBC listatenenciasJDBC = new TenenciaJDBC();
        listaTenencias = FXCollections.observableArrayList();
        for (Tenencia listaTenencia : listatenenciasJDBC.mostrarDatos()) {
            if(placa.equals(listaTenencia.getPlaca())){
                listaTenencias.add(listaTenencia);               
            }
            
        }  

        tcCaptura.setCellValueFactory(new PropertyValueFactory<>("linea_captura"));
        tcAdeudo.setCellValueFactory( new PropertyValueFactory<>("ejercicio"));
        tcImpuesto.setCellValueFactory( new PropertyValueFactory<>("impuesto"));
        tcDerechos.setCellValueFactory(new PropertyValueFactory<>("derechos"));
        tvTenencias.setItems(listaTenencias);
                
    }
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tvTenencias.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {this.tenencia = newValue;});
    }
    
    @FXML
    public void regresaMenu(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MenuFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    MenuFXMLController controlador = (MenuFXMLController)loader.getController();

	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }
    
    @FXML
    public void pagoEnLinea(){
        String et= tenencia.getLinea_captura();
        
        if(et.equals("PAGADA")){
            JOptionPane.showMessageDialog(null, "Esta multa ya fue pagada","Atención",JOptionPane.WARNING_MESSAGE);         
        }else{
            try{
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/FormaDePagoFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    FormaDePagoFXMLController controlador = (FormaDePagoFXMLController)loader.getController();
            
            controlador.reciboPlaca(placa);
            controlador.reciboCont(cont);
            controlador.reciboPrecio(pagoTotal());
            
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
            
            MultaJDBC multas = new MultaJDBC();
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
        }    
    }    
    
    @FXML
    public void tenenciaPagada() {
        
        String captura = tenencia.getLinea_captura();
        String e= tenencia.getLinea_captura();
        System.out.println("Cont vale * ---> "+cont+" estatus-->"+tenencia.getLinea_captura());
        System.out.println("Cont vale ---> "+cont+" Tengo e--->"+e);
        System.out.println("Pago total: "+pagoTotal());
        this.cont++;
    }
    
    public float pagoTotal(){
        float total = tenencia.getDerechos()+tenencia.getImpuesto();
        return total;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
        
}
