package controles;

import DaoModel.MultaJDBC;
import DtoModel.Multa;
import DtoModel.TarjetaCredito;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

public class PagoTarjetaFXMLController implements Initializable {

    @FXML
    private AnchorPane ap;
    @FXML
    private Label lbTitulo;
    @FXML
    private ChoiceBox<String> cbMes;
    @FXML
    private Label Lebel;
    @FXML
    private Label lbPrecio;
    @FXML
    private TextField tfTitular;
    @FXML
    private TextField tfTarjeta;
    @FXML
    private TextField tfAnio;
    @FXML
    private PasswordField pfCvv;
    @FXML
    private Button btnPagar;
    @FXML
    private Button btnRegresar;
    
    Date date = new Date();
    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    int month = localDate.getMonthValue();
    
    private final String[] mes = {"01-Ene","02-Feb","03-Mar","04-Abr","05-May","06-Jun",
                                  "07-Jul","08-Ago","09-Sep","10-Oct","11-Nov","12-Dic"};
    Multa multa;
    
    private String placa;
    int cont,fl;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        llenaCbox();
    }
    
    @FXML
    public void regresaVentana(){
        try{        
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/FormaDePagoFXML.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();	    	
	    FormaDePagoFXMLController controlador = (FormaDePagoFXMLController)loader.getController();
                        
            //controlador.cargaTabla(multa.getPlaca().toUpperCase());
	    ap.getChildren().clear();
	    ap.getChildren().add(root);
			      
	    } catch (IOException e) {
		e.printStackTrace();
	    }
    }

    public void multaPagada(){
                MultaJDBC actualizaMulta = new MultaJDBC();
                actualizaMulta.actualizarMulta(fl);
                regresaVentanaListaMultas();
    }

    public void regresaVentanaListaMultas() {
        try {
            
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/MultasFXML.fxml"));
                Parent root = loader.load();
                MultasFXMLController controlador = loader.getController();

                controlador.cargaTabla(placa);
                controlador.reciboCont(cont);
                
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
                Stage nuevoStage = (Stage) btnPagar.getScene().getWindow();
                nuevoStage.close();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    public void regresaVentanaListaTenencias() {
        try {
            
           FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/TenenciasFXML.fxml"));
                Parent root = loader.load();
                TenenciasFXMLController controlador = loader.getController();

                controlador.cargaTabla(placa);
                controlador.reciboCont(cont);
                
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
                Stage nuevoStage = (Stage) btnPagar.getScene().getWindow();
                nuevoStage.close();
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
   
    public void llenaCbox(){
        //Agrega meses al ChoiceBox
        cbMes.getItems().addAll(mes);
       //Se agrega un titulo al choiceBox
        Platform.runLater(() -> {
        SkinBase<ChoiceBox<String>> skin = (SkinBase<ChoiceBox<String>>) cbMes.getSkin();
        for (Node child : skin.getChildren()) {
            
            if (child instanceof Label) {
                
                Label label = (Label) child;
                if (label.getText().isEmpty()) {
                    label.setText("Mes");
                }
                return;
            }
        }
    });    
}
    @FXML
    public void validaTarjeta(){
        TarjetaCredito v = new TarjetaCredito();
         
        if(v.validarNombre(tfTitular.getText()) && v.validarTarjeta(tfTarjeta.getText())
           && v.validarAnio(tfAnio.getText()) && !cbMes.getSelectionModel().isEmpty()
           && v.validarCvv(pfCvv.getText()) && caducoTarjeta() == false)
        {  
            JOptionPane.showMessageDialog(null, "Procesando pago","Sistema de Pagos",JOptionPane.WARNING_MESSAGE);
            multaPagada();
            
        }if(tfTitular.getText().isEmpty() || v.validarNombre(tfTitular.getText()) == false){
            tfTitular.setText(null);
            tfTitular.setStyle("-fx-prompt-text-fill: red;");
            tfTitular.setPromptText("*Campo incorrecto");
        }if(tfTarjeta.getText().isEmpty() || v.validarTarjeta(tfTarjeta.getText()) == false){ 
            tfTarjeta.setText(null);
            tfTarjeta.setStyle("-fx-prompt-text-fill: red;");
            tfTarjeta.setPromptText("*Campo incorreto");
        }if(cbMes.getSelectionModel().isEmpty()){
            JOptionPane.showMessageDialog(null, "Debe seleccionar el mes","Sistema de Pagos",JOptionPane.WARNING_MESSAGE);
        }if(caducoTarjeta() == true){
            JOptionPane.showMessageDialog(null,"La tarjeta caduco","Sistema de Pagos",JOptionPane.WARNING_MESSAGE);
        }
        if(tfAnio.getText().isEmpty() || v.validarAnio(tfAnio.getText()) == false){
            tfAnio.setText(null);
            tfAnio.setStyle("-fx-prompt-text-fill: red;");
            tfAnio.setPromptText("*Campo incorrecto");
        }if(pfCvv.getText().isEmpty() || v.validarCvv(pfCvv.getText()) == false){
            pfCvv.setText(null);
            pfCvv.setStyle("-fx-prompt-text-fill: red;");
            pfCvv.setPromptText("*Campo incorrecto");
        }
    }
    
    private boolean caducoTarjeta(){
        String numeroMes = cbMes.getValue().substring(0,2);
        int numeroEntero = Integer.valueOf(numeroMes);
        
        String anio = tfAnio.getText();
        int anioEntero = Integer.valueOf(anio);
        
        if(numeroEntero<month && anioEntero == 22){
            return true;
        }else{
            return false;
        }  
    }
    
    public void reciboPlaca(String p){
        this.placa=p;
    }
    
    public void reciboCont(int c){
        this.cont=c;
    }
    
    public void lbPrecio(String lbPrecio) {
        this.lbPrecio.setText("$ "+lbPrecio);
    }
    
    public void reciboFolio(int f){
        this.fl =f;
    }
    
}
