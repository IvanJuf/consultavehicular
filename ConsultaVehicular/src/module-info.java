module ConsultaVehicular {
	requires javafx.controls;
	requires javafx.fxml;
	requires java.sql;
	requires java.desktop;
	requires org.apache.pdfbox;
	
	opens application to javafx.graphics, javafx.fxml;
}
