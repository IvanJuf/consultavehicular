package DtoModel;

import controles.CitasFXMLController;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;


public class Cita {
    
    CitasFXMLController c = new CitasFXMLController();
    
    public void creaCita(String d,String v,String h,String fe){
        File f;
        FileWriter w;
        BufferedWriter bw;
        PrintWriter wr;
        
        try {
            f = new File("cita.txt");
            w = new FileWriter(f);
            bw = new BufferedWriter(w);
            wr = new PrintWriter(bw);
            
            wr.write("***********************************************************");
            wr.write("\n                    COMPROBANTE DE CITA         ");
            wr.append("\n                    CONSULTA DE MULTAS");
            wr.append("\n***********************************************************\n");
            wr.append("\nDelegación:"+d);
            wr.append("\nVerificentro: "+v);
            wr.append("\nHora: "+h);
            wr.append("\n-----------------------------------------------------------\n");
            wr.append("\nFecha de la cita:"+fe);
            wr.append("\n-----------------------------------------------------------");
            wr.append("\n                       DevSolutions");
            wr.append("\n                   devsolution@gmail.com");
            wr.append("\n                    Tel: 55-33-22-11-00");
            wr.append("\n-----------------------------------------------------------");
            wr.close();
            bw.close();  
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }
    
    public void crearPDF(String dele,String vc,String hr,String fech){
        try {
            PDDocument documento = new PDDocument();
            //Crea una pagina en blanco dentro de nuestro archivo, A6 es el tamaño de pag
            PDPage pagina = new PDPage(PDRectangle.A6);
            documento.addPage(pagina);
            PDPageContentStream contenido = new PDPageContentStream(documento,pagina);
                        
                contenido.beginText();
                contenido.setFont(PDType1Font.TIMES_BOLD,9);
                contenido.setLeading(14.5f);
                contenido.setNonStrokingColor(Color.BLUE);
                contenido.newLineAtOffset(20, pagina.getMediaBox().getHeight()-52);

                String text1 = "                                COMPROBANTE DE CITAS";
                String text2 = "                                  CONSULTA DE MULTAS";
                String text3 = "";
                String text4 = "                                              DevSolutions";
                String text5 = "                                    devsolutions@gmail.com";
                String text6 = "                                         Tel: 55-00-11-22-33";
                
                contenido.showText(text1);
                contenido.newLine();
                contenido.showText(text2);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText("DELEGACIÓN: ");
                contenido.newLine();
                contenido.showText(dele);
                contenido.newLine();
                //contenido.showText(text3);
                //contenido.newLine();
                contenido.showText("VERIFICENTRO: ");
                contenido.newLine();
                contenido.showText(vc);
                contenido.newLine();
                //contenido.showText(text3);
                //contenido.newLine();
                contenido.showText("HORA: ");
                contenido.newLine();
                contenido.showText(hr);
                contenido.newLine();
                //contenido.showText(text3);
                //contenido.newLine();
                contenido.showText("FECHA: ");
                contenido.newLine();
                contenido.showText(fech);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText(text3);
                contenido.newLine();
                contenido.showText(text4);
                contenido.newLine();
                contenido.showText(text5);
                contenido.newLine();
                contenido.showText(text6);
                
                contenido.endText();
            //}
                contenido.close();
                documento.save("C:\\Users\\ivano\\OneDrive\\Escritorio\\cita.pdf");
            
        } catch (Exception e) {
            System.out.println("Error: "+e.getMessage().toString());
        }
    }
}
