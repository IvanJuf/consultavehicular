package DtoModel;

public class Tenencia {
    
    private String linea_captura;
    private String placa;
    private int ejercicio;
    private int impuesto;
    private int derechos;

    public Tenencia(String linea_captura, String placa, int ejercicio, int impuesto, int derechos) {
        this.linea_captura = linea_captura;
        this.placa = placa;
        this.ejercicio = ejercicio;
        this.impuesto = impuesto;
        this.derechos = derechos;
    }

    public String getLinea_captura() {
        return linea_captura;
    }

    public void setLinea_captura(String linea_captura) {
        this.linea_captura = linea_captura;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }

    public int getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(int impuesto) {
        this.impuesto = impuesto;
    }

    public int getDerechos() {
        return derechos;
    }

    public void setDerechos(int derechos) {
        this.derechos = derechos;
    }

    @Override
    public String toString() {
        return "Tenencia{" + "linea_captura=" + linea_captura + ", placa=" + placa + 
                ", ejercicio=" + ejercicio + ", impuesto=" + impuesto + ", derechos=" + derechos + '}';
    }


    
    
    
}
