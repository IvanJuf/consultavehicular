package DtoModel;

import controles.Tarjeta;

public class TarjetaCredito implements Tarjeta {
    
    private String titular;
    private String numeroTarjeta;
    private String mes;
    private int anio;
    private String cvv;
    

    @Override
    public boolean validarNombre(String nombre) {
        return nombre.matches("^[A-ZÁÉÍÓÚa-záéíóú]+[ ][A-ZÁÉÍÓÚa-záéíóú]+[ ][A-ZÁÉÍÓÚa-záéíóú]+[ ]?([A-ZÁÉÍÓÚa-záéíóú]+)?$");                
    }

    @Override
    public boolean validarTarjeta(String tarjeta) {
        return tarjeta.matches("^[4-5]+[0-679][0-9]{2}+([-]?+[0-9]{4}){3}$");
    }

    @Override
    public boolean validarAnio(String anio) {
        return anio.matches("^[2][1-9]$");
    }

    @Override
    public boolean validarCvv(String cvv) {
        return cvv.matches("^[0-9]{3}$");
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
    
    
}
















    /*
     public static boolean validarNombre(String nombre){
        return nombre.matches("^[A-ZÁÉÍÓÚa-záéíóú]+[ ][A-ZÁÉÍÓÚa-záéíóú]+[ ][A-ZÁÉÍÓÚa-záéíóú]+[ ]?([A-ZÁÉÍÓÚa-záéíóú]+)?$");                
    }
    
    public static boolean validarTarjeta(String tarjeta){
        return tarjeta.matches("^[4-5]+[0-679][0-9]{2}+([-]?+[0-9]{4}){3}$");  
    }
    
    public static boolean validarAnio(String anio){
        return anio.matches("^[2][1-9]$");
    }
    
    public static boolean validarCvv(String cvv){
        return cvv.matches("^[0-9]{3}$");
    }  
    */