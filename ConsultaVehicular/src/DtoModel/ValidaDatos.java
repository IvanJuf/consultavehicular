package DtoModel;

public class ValidaDatos {
    
    public static boolean validarCorreo(String correo){
        return correo.matches("^[A-ZÁÉÍÓÚa-záéíóú0-9]+[_-]?([_-]?[A-ZÁÉÍÓÚa-záéíóú0-9_-]+)?[@][a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$$");                
    }
    
    public static boolean validarPassword(String password){
        return password.matches("^([0-9]+)?[A-ZÁÉÍÓÚa-záéíóú0-9]+$");  
    }
    
}
